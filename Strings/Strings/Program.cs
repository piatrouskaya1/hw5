﻿using System;
using System.Text.RegularExpressions;

class Program
{
    static void Main()
    {
        //Task 1
        Console.WriteLine("Enter the string");
        string inputString = Console.ReadLine();
        string replacedString = inputString.Replace("test", "testing");
        string resultString = Regex.Replace(replacedString, @"\d", "");
        Console.WriteLine(resultString);

        //Task 2
        string word1 = "Welcome";
        string word2 = "to";
        string word3 = "the";
        string word4 = "TMS";
        string word5 = "lessons";

        string result = $"\"{word1}\" \"{word2}\" \"{word3}\" \"{word4}\" \"{word5}\"";

        Console.WriteLine(result);

        //Task 3
        string inputString3 = "teamwithsomeofexcersicesabcwanttomakeitbetter";

        int index = inputString3.IndexOf("abc");

        if (index != -1)
        {
            string beforeABC = inputString3.Substring(0, index);
            string afterABC = inputString3.Substring(index + 3);

            Console.WriteLine("First varible " + beforeABC);
            Console.WriteLine("Second varible " + afterABC);
        }
        else
        {
            Console.WriteLine("String 'abc' is not found");
        }

        //Task 4
        string inputString4 = "bad day";

        string stringWithoutBad = inputString4.Replace("bad", "").Trim();

        string newString = stringWithoutBad.Insert(stringWithoutBad.Length, "good day").Insert(stringWithoutBad.Length + 12, "!!!!!!!!!");

        newString = newString.Substring(0, newString.LastIndexOf("!")) + "?" + newString.Substring(newString.LastIndexOf("!") + 1);

        Console.WriteLine(newString);
    }
}