﻿using System;

class DocumentCheck
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Enter document number with format xxxx-yyy-xxxx-yyy-xyxy where x is a number and y is a letter");
        string documentNumber = Console.ReadLine();

        DisplayFirstTwoBlocks(documentNumber);

        string maskedDocumentNumber = MaskLetterBlocks(documentNumber);
        Console.WriteLine("Document number with *** " + maskedDocumentNumber);

        bool containsAbc = ContainsAbc(documentNumber);
        Console.WriteLine("Document number with abc  " + containsAbc);

        bool startsWith555 = StartsWith555(documentNumber);
        Console.WriteLine("Document number with 555 " + startsWith555);

        bool endsWith1a2b = EndsWith1a2b(documentNumber);
        Console.WriteLine("Document ends with 1a2b: " + endsWith1a2b);
    }

    public static void DisplayFirstTwoBlocks(string documentNumber)
    {
        string[] blocks = documentNumber.Split('-');
        if (blocks.Length >= 2)
        {
            Console.WriteLine($"First 2 blocks: {blocks[0]}-{blocks[1]}");
        }
        else
        {
            Console.WriteLine("Incorrect document number");
        }
    }

    public static string MaskLetterBlocks(string documentNumber)
    {
        string[] blocks = documentNumber.Split('-');
        for (int i = 0; i < blocks.Length; i++)
        {
            if (HasLetters(blocks[i]))
            {
                blocks[i] = "***";
            }
        }
        return string.Join("-", blocks);
    }

    public static bool ContainsAbc(string documentNumber)
    {
        return documentNumber.IndexOf("abc", StringComparison.OrdinalIgnoreCase) != -1;
    }

    public static bool StartsWith555(string documentNumber)
    {
        return documentNumber.StartsWith("555");
    }

    public static bool EndsWith1a2b(string documentNumber)
    {
        return documentNumber.EndsWith("1a2b", StringComparison.OrdinalIgnoreCase);
    }

    private static bool HasLetters(string input)
    {
        foreach (char c in input)
        {
            if (char.IsLetter(c))
            {
                return true;
            }
        }
        return false;
    }
}